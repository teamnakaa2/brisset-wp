install.bdd:
	@echo '⚙️ Start install bdd';
	docker-compose up -d --force-recreate --no-deps --build bdd
	
install.wordpress:
	@echo '⚙️ Start install wordpress'
	docker-compose up -d --force-recreate --no-deps --build www

install.composer:
	@echo '⚙️ Start install composer'
	docker-compose run --rm composer bash -c "cd ../var/www/html/ && composer install"

install.theme:
	@echo '🖥 Start install theme'
	docker-compose run --rm cli bash -c "wp core config && wp core install && wp language core install fr_FR --activate --allow-root && wp rewrite structure && wp site empty --yes && wp theme activate App && wp plugin delete hello && wp plugin activate --all && wp theme delete twentynineteen && wp theme delete twentyseventeen && wp theme delete twentytwenty"

install.npm:
	@echo '🖥 Start install theme'
	npm install
	
install: install.bdd install.wordpress install.composer install.theme install.npm
	@echo '✅ The install is done';

dev:
	npm run dev
	@echo '✅ The build is readay for dev';

build:
	npm run build
	@echo '✅ The build is done';

exportbdd:
	docker-compose run --rm cli bash -c "wp db export ../Data/bdd/my_wordpress_db.sql"

exportyost:
	docker-compose run --rm cli bash -c "wp db export --tables=wp_yoast_indexable,wp_yoast_indexable_hierarchy ../Data/bdd/my_wordpress_db2.sql"

importbdd:
	docker-compose run --rm cli bash -c "wp db import ../Data/bdd/my_wordpress_db.sql"

exportre7: 
	docker-compose run --rm cli bash -c "wp search-replace 'http://localhost' 'https://nakaa.fr/test/brisset' --export='../Data/bdd/my_wordpress_db-re7.sql'"

image:
	@echo '🖥 Copy image'
	docker-compose run --rm cli bash -c "wp media import ../import-image/**.svg && wp media import ../import-image/images/**.png"