<?php
/**
 * Template Name: Contact Page
 */

$context = Timber::context();
$context['post'] = new Timber\Post();
$context['logo'] = get_field('logo', 'options');

Timber::render('contact.twig', $context);