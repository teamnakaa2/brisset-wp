<?php

$context = Timber::context();
$context['post'] = new Timber\Post();

Timber::render(
    array('page-' . $post->post_type . '.twig',
    'page.twig'), $context);