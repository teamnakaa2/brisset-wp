<?php
    $context = Timber::context();

$context['title'] = 'Page category';
$context['post'] = new Timber\Post();
// $context['navHeaderPills'] = new \Timber\Menu( 'nav-header-pills' );
// $context['navHeader'] = new \Timber\Menu( 'nav-header' );
// $context['navContact'] = new \Timber\Menu( 'nav-contact' );

// $context['navFooter1'] = new \Timber\Menu( 'nav-footer-1' );
// $context['navFooter2'] = new \Timber\Menu( 'nav-footer-2' );
// $context['navFooter3'] = new \Timber\Menu( 'nav-footer-3' );

$context['post'] = new Timber\Post();

$context['legalTheme'] = get_field('legal_theme', 'options');

Timber::render('../views/category.twig', $context);