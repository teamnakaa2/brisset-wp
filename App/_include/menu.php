<?php

register_nav_menus(array('nav-header-pills' => 'Nav header pills'));
register_nav_menus(array('nav-header' => 'Nav header'));
register_nav_menus(array('nav-header-mobile' => 'Nav header mobile'));
register_nav_menus(array('nav-contact' => 'Nav contact'));
register_nav_menus(array('nav-sticky' => 'Nav flottante'));

register_nav_menus(array('nav-footer-1' => 'Nav footer 1'));
register_nav_menus(array('nav-footer-2' => 'Nav footer 2'));
register_nav_menus(array('nav-footer-3' => 'Nav footer contact'));

register_nav_menus(array('nav' => 'Navigation principal'));
register_nav_menus(array('nav-category' => 'Navigation catégorie'));


add_filter( 'timber/context', 'add_menu_to_context' );

function add_menu_to_context( $context ) {
    // $context = Timber::context();

    $context['navHeaderPills'] = new \Timber\Menu( 'nav-header-pills' );
    $context['navHeader'] = new \Timber\Menu( 'nav-header' );
    $context['navHeaderMobile'] = new \Timber\Menu( 'nav-header-mobile' );
    $context['navSticky'] = new \Timber\Menu( 'nav-sticky' );

    $context['navFooter1'] = new \Timber\Menu( 'nav-footer-1' );
    $context['navFooter2'] = new \Timber\Menu( 'nav-footer-2' );
    $context['navFooter3'] = new \Timber\Menu( 'nav-footer-3' );

    $context['legalTheme'] = get_field('legal_theme', 'options');
    
    
    return $context;
}
