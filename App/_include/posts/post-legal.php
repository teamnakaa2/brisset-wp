<?php

function pagelegal_custom_post_type() {

$labels = array(
    'name'                => _x( 'Pages Légales', 'Post Type General Name'),
    'singular_name'       => _x( 'Page Légale', 'Post Type Singular Name'),
    'menu_name'           => __( 'Pages légales'),
    'all_items'           => __( 'Voir les pages'),
    'view_item'           => __( 'Voir les pages'),
    'add_new_item'        => __( 'Ajouter une page'),
    'add_new'             => __( 'Ajouter une page'),
    'edit_item'           => __( 'Editer le page'),
    'update_item'         => __( 'Modifier la page'),
    'search_items'        => __( 'Rechercher une page'),
    'not_found'           => __( 'Non trouvée'),
    'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
);


$args = array(
    'label'               => __( 'Pages légales'),
    'description'         => __( 'Liste des pages légales'),
    'labels'              => $labels,
    'menu_icon'           => 'dashicons-admin-page',
    'supports'            => array( 'title', 'editor', 'custom-fields', 'page-attributes' ),
    'show_in_rest' => true,
    'hierarchical'        => true,
    'public'              => true,
    'has_archive'         => true,
    'capability_type' => 'page',
    
);

register_post_type( 'pagelegale', $args );

}

add_action( 'init', 'pagelegal_custom_post_type', 0 );
