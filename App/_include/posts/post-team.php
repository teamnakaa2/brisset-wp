<?php

function team_custom_post_type() {

$labels = array(
    'name'                => _x( 'Équipes', 'Post Type General Name'),
    'singular_name'       => _x( 'Équipe', 'Post Type Singular Name'),
    'menu_name'           => __( 'Équipes'),
    'all_items'           => __( 'Voir l\'équipes'),
    'view_item'           => __( 'Voir l\'équipes'),
    'add_new_item'        => __( 'Ajouter une personne'),
    'add_new'             => __( 'Ajouter une personne'),
    'edit_item'           => __( 'Editer la personne'),
    'update_item'         => __( 'Modifier la page'),
    'search_items'        => __( 'Rechercher une personne'),
    'not_found'           => __( 'Non trouvée'),
    'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
);


$args = array(
    'label'               => __( 'Pages légales'),
    'description'         => __( 'Liste de l\'équipe'),
    'labels'              => $labels,
    'menu_icon'           => 'dashicons-admin-page',
    'supports'            => array( 'title', 'custom-fields' ),
    'show_in_rest' => true,
    'hierarchical'        => true,
    'public'              => false,
    'publicly_queryable' => true,  // you should be able to query it
'show_ui' => true, 
    'has_archive'         => true,
    'rewrite' => false,
);

register_post_type( 'team', $args );

}

add_action( 'init', 'team_custom_post_type', 1 );
