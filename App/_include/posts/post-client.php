<?php

function client_custom_post_type() {

	$labels = array(
		'name'                => _x( 'Clients', 'Post Type General Name'),
		'singular_name'       => _x( 'Client', 'Post Type Singular Name'),
		'menu_name'           => __( 'Client'),
		'all_items'           => __( 'Toutes les clients'),
		'view_item'           => __( 'Voir les clients'),
		'add_new_item'        => __( 'Ajouter un nouveau client'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer le client'),
		'update_item'         => __( 'Modifier le client'),
		'search_items'        => __( 'Rechercher un client'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	
	$args = array(
		'label'               => __( 'Client'),
		'description'         => __( 'Liste de vos clients'),
		'labels'              => $labels,
		'menu_icon'           => 'dashicons-groups',
		'supports'            => array( 'title', 'author', 'custom-fields' ),
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
	);
	
	register_post_type( 'client', $args );

}

add_action( 'init', 'client_custom_post_type', 2 );

