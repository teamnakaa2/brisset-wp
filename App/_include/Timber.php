<?php

$timber = new Timber\Timber();

Timber::$dirname = array('blocks','views');

add_filter( 'timber/context', 'add_to_context' );

function add_to_context( $context ) {
    
    $context['navHeaderPills'] = new \Timber\Menu( 'nav-header-pills' );
    $context['navHeader'] = new \Timber\Menu( 'nav-header' );
    $context['navContact'] = new \Timber\Menu( 'nav-contact' );

    $context['navFooter1'] = new \Timber\Menu( 'nav-footer-1' );
    $context['navFooter2'] = new \Timber\Menu( 'nav-footer-2' );
    $context['navFooter3'] = new \Timber\Menu( 'nav-footer-3' );

    $context['legalTheme'] = get_field('legal_theme', 'options');


    return $context;
}