<?php
// ADD THEME JS
function my_assets() {
  $devMode = false;
  wp_enqueue_script( 'script-default', get_path_css( "index.js", $devMode), null, null );
}
add_action( 'wp_footer', 'my_assets' );