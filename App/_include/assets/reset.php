<?php 
// REMOVE
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function remove_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    
}

function remove_scripts(){
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'remove_scripts' );
