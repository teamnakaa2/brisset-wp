<?php
// Function get hash of assets
function get_path_css( $css, $dev = false ) {
    $map = get_template_directory() . '/assets/manifest.json';
    static $hash = null;
    if ( null === $hash ) {
        $hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
    }
    if($dev) {
        if ( array_key_exists( $css, $hash ) ) {
            return ':5000/' . $hash[ $css ];
        } else {
          return ':5000/'.$css;
        }
    } else {
        if ( array_key_exists( $css, $hash ) ) {
            return '/wp-content/assets/' . $hash[ $css ];
        } else {
          return '/wp-content/assets/'.$css;
        }
    }
    return $css;
}
