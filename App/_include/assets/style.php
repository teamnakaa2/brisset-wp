<?php

// ADD THEME CSS

function theme_styles() {
  $devMode = false;
  wp_enqueue_style( 'style-global', get_path_css( "global.css", $devMode), null , null  );
  wp_enqueue_style( 'style-page', get_path_css( "index.css", $devMode), null , null  );
  wp_enqueue_style( 'style-block', get_path_css( "block-global.css", $devMode), null , null  );
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

// ADD ADMIN CSS
function admin_style() {
  $devMode = false;
  wp_enqueue_style( 'style-admin', get_path_css( "admin.css", $devMode), null , null  );
  wp_enqueue_style( 'style-block', get_path_css( "block-global.css", $devMode), null , null  );
}
add_action('admin_enqueue_scripts', 'admin_style');

// ADD LOGIN CSS
function login_style() {
  $devMode = false;
  wp_enqueue_style( 'login-styles', get_path_css( "admin.css", $devMode), null , null  );
}
add_action('login_enqueue_scripts', 'login_style');
