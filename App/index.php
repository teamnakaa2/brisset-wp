<?php 
    if(is_home()) {
        return require get_template_directory() . '/controller/blog.php';
    }
    if(is_front_page()) {
        return require get_template_directory() . '/controller/home.php';
    }
    if(is_page()) {
        return require get_template_directory() . '/controller/page.php';
    }
    if(is_archive()) {
        return require get_template_directory() . '/controller/archive.php';
    }
    if(is_single()) {
        return require get_template_directory() . '/controller/article.php';
    }
    if(is_search()) {
        return require get_template_directory() . '/controller/search.php';
    }
    if(is_category()) {
        return require get_template_directory() . '/controller/category.php';
    }

    return require get_template_directory() . '/controller/404.php';
