<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\ColorPicker;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'title' => 'config',
    'fields' => [
        Text::make('Title'),
        Image::make('Logo', 'logo'),
        Text::make('Mentions légales du théme', 'legal_theme'),
        ColorPicker::make('Text Color')
            ->instructions('Add text color.')
            ->defaultValue('#333'),
        ColorPicker::make('Color title')
            ->instructions('Add title color.')
            ->defaultValue('#263575'),
        Repeater::make('List color brand', 'my_color_values')
            ->instructions('List color brand.')
            ->fields([
                Text::make('Color name', 'color_label'),
                ColorPicker::make('New color brand', 'color_value')
                    ->instructions('Add brand color.')
                    ->required()
                    ->required(),
            ])
        ],
    'location' => [
        Location::if('options_page', 'theme-settings')
    ],
]);