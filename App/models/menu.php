<?php

use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Location;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'menu',
    'align' => false,
    'fields' => [
        Text::make('Ancre', 'anchor')
            ->instructions("Indiquer un hashtag précédé de '#'"),
    ],
    'label_placement' => 'top',
    'instruction_placement' => 'field',
    'position' => 'normal',
    'location' => [
        Location::if('nav_menu_item', 'all')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'menu_nav',
    'align' => false,
    'fields' => [
        Image::make('Picto', 'picto')
            ->instructions("Picto affiché devant le lien"),
    ],
    'label_placement' => 'top',
    'instruction_placement' => 'field',
    'position' => 'normal',
    'location' => [
        Location::if('nav_menu', 'location/nav-footer-3')
    ],
]);
