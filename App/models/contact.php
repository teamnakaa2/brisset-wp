<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
//use WordPlate\Acf\Fields\GoogleMap;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Accordion;
use WordPlate\Acf\Fields\Link;


register_extended_field_group([
    'title' => 'Page contact',
    'fields' => [
        Text::make('Titre de la page', 'page_title'),
        Text::make('Code du formulaire', 'code_form')
            ->instructions('Voir la page contact/formulaire de contact'),
        Accordion::make('Informations', 'info'),
        Image::make('Image', 'img'),
        Image::make('Visuel', 'visuel'),
//        Text::make('Adresse google maps', 'map'),
//        GoogleMap::make('Adresse', 'address')
//            ->instructions('Add the Google Map address.')
//            ->center(57.456286, 18.377716)
//            ->zoom(14)
//            ->required(),
        Repeater::make('Liste des informations', 'info_list')
            ->fields([
                Image::make('Icon', 'icon'),
                Text::make('Info', 'info'),
                Link::make('Lien', 'link'),
            ])
    ],
    'location' => [
        Location::if('page_template','==', 'controller/contact-page.php')
    ],
]);