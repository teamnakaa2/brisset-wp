<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'list-team',
    'align' => false,
    'fields' => [
        Image::make('Image', 'asset'),
        Text::make('Poste', 'poste'),
        Text::make('Lien linkedin', 'linkedin'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('post_type', 'team')
    ],
]);
