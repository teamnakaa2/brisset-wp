<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\ColorPicker;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'title' => 'Clients',
    'fields' => [
        Image::make('Logo', 'client_logo'),
        Text::make('Nom du client', 'client_name'),
        Text::make('Fonction du client', 'client_function'),
        Image::make('Image du client', 'client_img'),
        Text::make('Avis du client', 'client_avis'),
    ],
    'location' => [
        Location::if('post_type', 'client')
    ],
]);