<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Link;
use WordPlate\Acf\Fields\PostObject;
use WordPlate\Acf\Fields\Wysiwyg;
use WordPlate\Acf\Fields\Radio;
use WordPlate\Acf\Fields\ColorPicker;
use WordPlate\Acf\Fields\Textarea;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'text-asset',
    'align' => false,
    'fields' => [
        Image::make('Image', 'assets'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/text-asset')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'title-text-asset',
    'align' => false,
    'fields' => [
        Text::make('Anchor', 'anchor'),
        Image::make('Image', 'asset'),
        Group::make('Titre', 'title_grp')
            ->fields([
                Image::make('Image', 'title_asset'),
                Text::make('Titre', 'title_txt'),
            ])
            ->layout('row'),
        Repeater::make('Ligne de texte', 'list_text')
            ->fields([
                Text::make('Texte', 'text'),
            ])
            ->buttonLabel('Ajouter une nouvelle ligne')
            ->layout('block') // block, row or table
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/title-text-asset')
    ],
]);


register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'list-text',
    'align' => false,
    'fields' => [
        Image::make('Image', 'asset'),
        Repeater::make('Ligne de texte', 'list_text')
            ->fields([
                Text::make('Texte', 'text'),
            ])
            ->buttonLabel('Ajouter un novueau texte')
            ->layout('block') // block, row or table
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/list-text')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'list-step',
    'align' => false,
    'fields' => [
        Repeater::make('Liste étape', 'list_step')
            ->fields([
                Image::make('Image', 'asset'),
                Textarea::make('Texte', 'text')
                    ->newLines('br') // br or wpautop
                    ->characterLimit(300)
                    ->rows(3),
            ])
            ->buttonLabel('Ajouter une ligne')
            ->layout('block') // block, row or table
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/list-step')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'table-comparative',
    'align' => false,
    'fields' => [
        Repeater::make('Liste sans brisset', 'list_with')
            ->fields([
                Text::make('Texte', 'text'),
            ])
            ->buttonLabel('Ajouter une ligne')
            ->layout('block'),
        Repeater::make('Liste avec brisset', 'list_without')
            ->fields([
                Text::make('Texte', 'text'),
            ])
            ->buttonLabel('Ajouter une ligne')
            ->layout('block') 
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/table-comparative')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'slider-effect',
    'align' => false,
    'fields' => [
        Repeater::make('Slider', 'slider_effect')
            ->fields([
                Wysiwyg::make('Texte', 'text')
                    ->mediaUpload(false)
                    ->tabs('visual')
                    ->toolbar('simple'),
                ColorPicker::make('Couleur de fond', 'bgcolor')
                    ->defaultValue('#88BA5C')
                    ->required()
            ])
            ->buttonLabel('Ajouter une slide')
            ->layout('block'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/slider-effect')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'slider-client-logo',
    'align' => false,
    'fields' => [
        PostObject::make('Slider logo', 'slider_client_logo')
            ->postTypes(['client'])
            ->allowNull()
            ->allowMultiple()
            ->required()
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/slider-client-logo')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'slider-client-avis',
    'align' => false,
    'fields' => [
        PostObject::make('Slider client avis', 'slider_client_avis')
            ->postTypes(['client'])
            ->allowNull()
            ->allowMultiple()
            ->required()
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/slider-client-avis')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'list-equipe',
    'align' => false,
    'fields' => [
        PostObject::make('Liste de l\'équipe', 'list_equipe')
            ->postTypes(['team'])
            ->allowNull()
            ->allowMultiple()
            ->required(),
        Group::make('Postuler', 'postuler')
            ->fields([
                Image::make('Image', 'asset'),
                Text::make('Titre', 'title'),
                Text::make('Texte', 'text'),
                Link::make('Lien', 'url')
            ])
            ->layout('row'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/list-equipe')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'link',
    'align' => false,
    'fields' => [
        Link::make('Lien', 'url')
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/link')
    ],
]);

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'header',
    'align' => false,
    'fields' => [
        Radio::make('Type de header', 'header_type')
            ->instructions('Selectioner le style du header.')
            ->choices([
                'type_1' => 'Type 1',
                'type_2' => 'Type 2',
                'type_3' => 'Type 3'
            ])
            ->defaultValue('hotpink')
            ->returnFormat('value'), // value, label or array
        Image::make('Image', 'asset'),
        Image::make('Image mobile', 'asset_mobile'),
        Text::make('Titre', 'title'),
        Wysiwyg::make('Texte', 'text')
            ->mediaUpload(false)
            ->tabs('visual')
            ->toolbar('simple')
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/header')
    ],
]);