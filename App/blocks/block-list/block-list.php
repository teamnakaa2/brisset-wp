<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

use WordPlate\Acf\Fields\FlexibleContent;
use WordPlate\Acf\Fields\Layout;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Link;

use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Wysiwyg;
use WordPlate\Acf\Fields\Select;
use WordPlate\Acf\Fields\Accordion;

register_extended_field_group([
    // 'style' => 'seamless',
    'title' => 'Blocklist',
    'fields' => [
        Accordion::make('Title'),
            Select::make('Type')
                ->choices([
                    '2' => 'h2',
                    '3' => 'h3',
                    '4' => 'h4',
                    ]),
            Text::make('Titre'),
        Accordion::make('Content'),
            Wysiwyg::make('Texte')
    ],
    'supports' => array(
        'align' => array( 'left', 'right'),
    ),
    'label_placement' => 'top',
    'position' => 'side',
    'location' => [
        Location::if('block', 'acf/list-block')
    ],
]);

add_action( 'acf/init', 'my_list_init' );
function my_list_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'list_block',
        'title'           => __( 'List block', 'your-text-domain' ),
        'description'     => __( 'A list block.', 'your-text-domain' ),
        'render_callback' => 'my_list_block_render_callback',
        'category'        => 'list',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'list' ),
        'enqueue_style' => '/wp-content/assets/block-list.css',
    ) );
}

function my_list_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['listFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-list/block-list.twig', $context );
}
