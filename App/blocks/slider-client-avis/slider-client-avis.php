<?php
add_action( 'acf/init', 'slider_client_avis_init' );

function slider_client_avis_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'slider_client_avis',
        'title'           => __( 'Slider avis client', 'your-text-domain' ),
        'render_callback' => 'slider_client_avis_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function slider_client_avis_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $slider_client_avis = get_fields();
    $context['slider_client_avis'] = $slider_client_avis['slider_client_avis'];

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/slider-client-avis/slider-client-avis.twig', $context );
}