<?php
add_action( 'acf/init', 'list_equipe_init' );

function list_equipe_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'list_equipe',
        'title'           => __( 'Liste de l\'équipe', 'your-text-domain' ),
        'render_callback' => 'list_equipe_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function list_equipe_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $list_equipe = get_field('list_equipe');
    $context['list_equipe'] = $list_equipe;

    $postuler = get_field('postuler');
    $context['postuler'] = $postuler;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/list-equipe/list-equipe.twig', $context );
}