<?php

use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\ColorPicker;
use WordPlate\Acf\Fields\ButtonGroup;

register_extended_field_group([
    'title' => 'HeaderSmall',
    'style' => 'seamless',
    'label_placement' => 'top',
    'supports' => array(
        'align' => false,

    ),
    'align' => false,
    'fields' => [
        Image::make('Image', 'img'),
        Text::make('Titre', 'title')
            ->required(),
        ColorPicker::make('Couleur du titre', 'color_title')
            ->instructions('Ajouter une couleur au titre.')
            ->defaultValue('#273575'),
        ButtonGroup::make('Alignement', 'align_title')
            ->instructions('Alignement du titre en version desktop.')
            ->choices([
                'left' => 'A gauche',
                'center' => 'Centré',
                'right' => 'A droite',
            ])
            ->defaultValue('center')
            ->returnFormat('value') // value, label or array
            ->required()
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/header-small-block')
    ],
]);

add_action( 'acf/init', 'my_header_small_init' );
function my_header_small_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'header_small_block',
        'title'           => __( 'Header Small Block', 'your-text-domain' ),
        'description'     => __( 'A small header block.', 'your-text-domain' ),
        'render_callback' => 'my_header_small_render_callback',
        'category'        => 'formatting',
        'icon'            => 'embed-generic',
        'keywords'        => array( 'header', 'entete' ),
        'example' => array(
            'attributes'		=> array(
                'mode'			=> 'preview',
//                'data'			=> array(
//                    'content' 	=> __('<img src="https://via.placeholder.com/680x720">'),
//                ),
            )
        ),
    ) );
}

function my_header_small_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['headerSmallFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-header-small/block-header-small.twig', $context );
}