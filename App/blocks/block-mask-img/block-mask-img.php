<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockMaskImg',
    'fields' => [
        Image::make('Mask', 'mask'),
        Image::make('Picture', 'img'),
    ],
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/mask-img-block')
    ],
]);

add_action( 'acf/init', 'my_mask_img_init' );

function my_mask_img_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'mask_img_block',
        'title'           => __( 'Mask Image Block', 'your-text-domain' ),
        'description'     => __( 'A mask img block.', 'your-text-domain' ),
        'render_callback' => 'my_mask_img_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'mask', 'image' ),
        'enqueue_style' => '/wp-content/assets/block-mask-img.css',

    ) );
}

function my_mask_img_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    // $context['block'] = $block;

    // Store field values.
    $context['maskImgFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-mask-img/block-mask-img.twig', $context );
}