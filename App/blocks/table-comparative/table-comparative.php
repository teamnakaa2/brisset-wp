<?php
add_action( 'acf/init', 'table_comparative_init' );

function table_comparative_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'table_comparative',
        'title'           => __( 'Tableau comparatif', 'your-text-domain' ),
        'render_callback' => 'table_comparative_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function table_comparative_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $table_comparative = get_fields();
    $context['table_comparative'] = $table_comparative;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/table-comparative/table-comparative.twig', $context );
}