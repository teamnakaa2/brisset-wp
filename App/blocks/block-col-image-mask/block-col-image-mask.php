<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Link;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockColImageMask',
    'supports' => array( 
        'align' => false,
     ),
     'align' => false,
    'fields' => [
        Text::make('Titre', 'name'),
        Link::make('Lien', 'link'),
        Text::make('Texte', 'text'),
        Group::make('Image')
            ->fields([
                Image::make('Mask', 'mask'),
                Image::make('Picture', 'img'),
            ])
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/col-image-mask-block')
    ],
]);

add_action( 'acf/init', 'my_col_image_mask_init' );
function my_col_image_mask_init() {

    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'col_image_mask_block',
        'title'           => __( 'Col image mask Block', 'your-text-domain' ),
        'description'     => __( 'A col image mask block.', 'your-text-domain' ),
        'render_callback' => 'my_col_image_mask_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
        'enqueue_style' => '/wp-content/assets/block-col-image-mask.css',
    ) );
}

function my_col_image_mask_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();
    // Store block values.
    $context['block'] = $block;
    
    // Store field values.
    $context['colImageMaskFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-col-image-mask/block-col-image-mask.twig', $context );
}