<?php
add_action( 'acf/init', 'title_text_asset_init' );

function title_text_asset_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'title_text_asset',
        'title'           => __( 'Title text asset Block', 'your-text-domain' ),
        'render_callback' => 'title_text_asset_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function title_text_asset_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['title_text_asset'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/title-text-asset/title-text-asset.twig', $context );
}