<?php
add_action( 'acf/init', 'list_step_init' );

function list_step_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'list_step',
        'title'           => __( 'List step Block', 'your-text-domain' ),
        'render_callback' => 'list_step_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function list_step_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['list_step'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/list-step/list-step.twig', $context );
}