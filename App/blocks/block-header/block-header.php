<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

use WordPlate\Acf\Fields\FlexibleContent;
use WordPlate\Acf\Fields\Layout;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Link;

use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Wysiwyg;
use WordPlate\Acf\Fields\Select;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'Blockheader',
    'fields' => [
        // Text::make('Titre'),
        Group::make('Titre')
            ->fields([
                Select::make('Type')
                ->choices([
                    'h2' => 'h2',
                    'h3' => 'h3',
                    'h4' => 'h4',
                ]),
                Text::make('Text')
            ])
            ->layout('row'),
        FlexibleContent::make('Components', 'page-components')
            ->instructions('Add the employees occupation.')
            ->buttonLabel('Add a page component')
            ->layouts([
                Layout::make('Image')
                    ->layout('block')
                    ->fields([
                        Text::make('Description')
                    ])
            ])
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/header-block')
    ],
]);

add_action( 'acf/init', 'my_header_init' );
function my_header_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'header_block',
        'title'           => __( 'header block', 'your-text-domain' ),
        'description'     => __( 'A header block.', 'your-text-domain' ),
        'render_callback' => 'my_header_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'embed-generic',
        'keywords'        => array( 'header', 'entete' ),
        'enqueue_style' => '/wp-content/assets/block-header.css',
        'enqueue_script' => '/wp-content/assets/block-header.js',

    ) );
}

function my_header_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['headerFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-header/block-header.twig', $context );
}