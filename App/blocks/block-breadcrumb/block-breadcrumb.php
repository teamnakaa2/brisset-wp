<?php

add_action( 'acf/init', 'block_breadcrumb_init' );

function block_breadcrumb_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }
    // Register a new block.
    acf_register_block( array(
        'name'            => 'block_breadcrumb',
        'title'           => __( 'Fil ariane Yoast', 'your-text-domain' ),
        'description'     => __( 'Affichage du fil d ariane de Yoast', 'your-text-domain' ),
        'render_callback' => 'my_block_breadcrumb',
        'category'        => 'formatting',
        'icon'            => 'shortcode',
        'keywords'        => array( 'breadcrumb' ),
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        ),
        'example' => array(
            'attributes'		=> array(
                'mode'			=> 'preview',
//                'data'			=> array(
//                    'content' 	=> __('<img src="https://via.placeholder.com/680x720">'),
//                ),
            )
        ),
    ));
}

function my_block_breadcrumb( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();
    // Store block values.
    $context['block'] = $block;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-breadcrumb/block-breadcrumb.twig', $context );
}