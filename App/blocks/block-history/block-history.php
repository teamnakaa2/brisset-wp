<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockHistory',
    'supports' => array( 
        'align' => false,

     ),
     'align' => false,
    'fields' => [
        Repeater::make('Liste historique', 'list_history')
            ->fields([
                Text::make('Titre', 'title'),
                Text::make('Texte', 'text'),
            ])
            ->buttonLabel('Ajouter un élément')
            ->layout('block') // block, row or table
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/history-block')
    ],
]);

add_action( 'acf/init', 'my_history_block_init' );
function my_history_block_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'history_block',
        'title'           => __( 'History Block', 'your-text-domain' ),
        'description'     => __( 'A history block.', 'your-text-domain' ),
        'render_callback' => 'my_history_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
        'enqueue_style' => '/wp-content/assets/block-history.css',
    ) );
}

function my_history_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['historyFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-history/block-history.twig', $context );
}