<?php

use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Wysiwyg;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockWysiwyg',
    'supports' => array(
        'align' => false,

    ),
    'align' => false,
    'fields' => [
        Wysiwyg::make('Texte', 'text')
            ->mediaUpload(false)
            ->tabs('visual')
            ->toolbar('simple'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/block-wysiwyg')
    ],
]);

add_action( 'acf/init', 'my_block_wysiwyg_init' );
function my_block_wysiwyg_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }
    // Register a new block.
    acf_register_block( array(
        'name'            => 'block_wysiwyg',
        'title'           => __( 'Block WYSIWYG', 'your-text-domain' ),
        'render_callback' => 'my_block_wysiwyg_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' )
    ) );
}

function my_block_wysiwyg_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['wysiwig'] = get_field('text');

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-wysiwyg/block-wysiwyg.twig', $context );
}