<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Link;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockForm',
    
    'align' => false,
    'fields' => [
        Text::make('Code du formulaire', 'code_form')
            ->instructions('Voir la page contact/formulaire de contact'),
        Image::make('Image', 'img'),
        Link::make('Lien page contact mobile', 'mobile_link'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/form-block')
    ],
]);

add_action( 'acf/init', 'my_form_block_init' );
function my_form_block_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'form_block',
        'title'           => __( 'Col form Block', 'your-text-domain' ),
        'description'     => __( 'A form block.', 'your-text-domain' ),
        'render_callback' => 'my_form_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
        'supports'			=> array(
            'align' => true,
            'mode' => false,
            'jsx' => true
        ),
//        'enqueue_style' => '/wp-content/assets/block-form.css',
    ) );
}

function my_form_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['fromFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    $context['logo'] = get_field('logo', 'options');
    
    // Render the block.
    Timber::render( './blocks/block-form/block-form.twig', $context );
}