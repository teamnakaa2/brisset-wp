<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Number;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Group;

register_extended_field_group([
    'title' => 'BlockGridMasonry',
    'fields' => [
        Group::make('Animations')
            ->instructions('Ajuster les animations de la grille masonry')
            ->fields([
                Number::make('Durée de l\'animation', 'animation_duration')
                    ->min(0)
                    ->max(5000)
                    ->required(),
                Number::make('Décalage entre les animations', 'animation_delay')
                    ->min(0)
                    ->max(5000)
                    ->required(),
            ])
            ->layout('block')
            ->required(),
        Repeater::make('Liste', 'grid_masonry')
            ->fields([
                Image::make('Image', 'img'),
                Image::make('Picto', 'icon'),
                Text::make('Titre', 'title'),
            ])
            ->buttonLabel('Ajouter un élément')
            ->layout('block') // block, row or table
    ],
    'location' => [
        Location::if('block', 'acf/grid-masonry-block')
    ],
]);

add_action( 'acf/init', 'my_grid_masonry_block_init' );
function my_grid_masonry_block_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'grid_masonry_block',
        'title'           => __( 'Grid masonry Block', 'your-text-domain' ),
        'description'     => __( 'A grid masonry block.', 'your-text-domain' ),
        'render_callback' => 'my_grid_masonry_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
//        'enqueue_style' => '/wp-content/assets/block-grid-masonry.css',
    ) );
}

function my_grid_masonry_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['gridMasonryFields'] = get_fields();


    // Store $is_preview value.
    $context['is_preview'] = $is_preview;
    
    $context['logo'] = get_field('logo', 'options');

    // Render the block.
    Timber::render( './blocks/block-grid-masonry/block-grid-masonry.twig', $context );
}

