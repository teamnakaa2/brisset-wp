<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

use WordPlate\Acf\Fields\FlexibleContent;
use WordPlate\Acf\Fields\Layout;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Link;

use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Wysiwyg;
use WordPlate\Acf\Fields\Select;
use WordPlate\Acf\Fields\Accordion;

register_extended_field_group([
    // 'style' => 'seamless',
    'title' => 'Blocksection',
    'fields' => [
        Accordion::make('Configs'),
        Text::make('ID'),
        Accordion::make('Titre'),
            Select::make('Type')
            ->choices([
                '2' => 'h2',
                '3' => 'h3',
                '4' => 'h4',
                ]),
            Select::make('Color', 'color')
                ->choices(['2' => 'h2',]),
            Text::make('Text'),

        Accordion::make('Content'),
        FlexibleContent::make('Components', 'page_components')
            ->buttonLabel('Add a page component')
            ->layouts([
                Layout::make('Image')
                    ->layout('block')
                    ->fields([
                        Text::make('Description')
                    ]),
                Layout::make('Text')
                    ->layout('block')
                    ->fields([
                        Text::make('Text')
                    ])
            ])
    ],
    'supports'			=> array(
        'align' => true,
        'mode' => false,
        'jsx' => true
    ),
    'label_placement' => 'top',
    'position' => 'side',
    'location' => [
        Location::if('block', 'acf/section-block')
    ],
]);

add_action( 'acf/init', 'my_section_init' );
function my_section_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'section_block',
        'title'           => __( 'section block', 'your-text-domain' ),
        'description'     => __( 'A section block.', 'your-text-domain' ),
        'render_callback' => 'my_section_block_render_callback',
        'category'        => 'section',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'section' ),
        'enqueue_style' => '/wp-content/assets/block-section.css',
        'enqueue_script' => '/wp-content/assets/block-section.js',

    ) );
}

function my_section_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['sectionFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-section/block-section.twig', $context );
}


function section_block_categories( $categories ) {
    $category_slugs = wp_list_pluck( $categories, 'slug' );
    return in_array( 'section', $category_slugs, true ) ? $categories : array_merge(
        $categories,
        array(
            array(
                'slug'  => 'section',
                'title' => __( 'Section', 'section' ),
                'icon'  => null,
            ),
        )
    );
}
add_filter( 'block_categories_all', 'section_block_categories' );


function acf_load_color_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();
    // var_dump("dazdad");


    // if has rows
    if( have_rows('my_color_values', 'option') ) {
        
        // while has rows
        while( have_rows('my_color_values', 'option') ) {
            
            // instantiate row
            the_row();
            
            
            // vars
            $value = get_sub_field('color_value');
            $label = get_sub_field('color_label');

            // var_dump("dazdad");
            // append to choices
            $field['choices'][ $value ] = $label;
            
        }
        
    }


    // return the field
    return $field;
    
}

add_filter('acf/load_field/name=color', 'acf_load_color_field_choices');