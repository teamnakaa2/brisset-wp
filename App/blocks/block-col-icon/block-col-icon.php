<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Wysiwyg;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockColIcon',
    'supports' => array( 
        'align' => false,

    ),
    'align' => false,
    'fields' => [
        Repeater::make('Liste avec des pictos', 'list_icon')
            ->fields([
                Image::make('Picto', 'img'),
                Text::make('Titre', 'name'),
                Wysiwyg::make('Texte', 'text')
                    ->mediaUpload(false)
                    ->tabs('visual')
                    ->toolbar('simple')
                    ->required(),
            ])
            ->min(3)
            ->max(3)
            ->layout('block') // block, row or table
            ->required()
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/col-icon-block')
    ],
]);

add_action( 'acf/init', 'my_col_icon_block_init' );
function my_col_icon_block_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'col_icon_block',
        'title'           => __( 'Col icon Block', 'your-text-domain' ),
        'description'     => __( 'A col icon block.', 'your-text-domain' ),
        'render_callback' => 'my_col_icon_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
//        'enqueue_style' => '/wp-content/assets/block-col-icon.css',
    ) );
}

function my_col_icon_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['colIconFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-col-icon/block-col-icon.twig', $context );
}