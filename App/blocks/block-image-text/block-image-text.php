<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;
use WordPlate\Acf\Fields\Group;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockImageText',
    'supports' => array( 
        'align' => false,

     ),
     'align' => false,
    'fields' => [
        Image::make('Image', 'img'),
        Group::make('Titre picto', 'title_picto')
            ->fields([
                Image::make('Picto', 'icon'),
                Text::make('Titre', 'title'),
            ]),
        Text::make('Texte', 'text'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/image-text-block')
    ],
]);

add_action( 'acf/init', 'my_image_text_block_init' );
function my_image_text_block_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'image_text_block',
        'title'           => __( 'Image text Block', 'your-text-domain' ),
        'description'     => __( 'A image text block.', 'your-text-domain' ),
        'render_callback' => 'my_image_text_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
        'enqueue_style' => '/wp-content/assets/block-image-text.css',
    ) );
}

function my_image_text_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['imageTextFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-image-text/block-image-text.twig', $context );
}