<?php
add_action( 'acf/init', 'link_init' );

function link_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'link',
        'title'           => __( 'Lien', 'your-text-domain' ),
        'render_callback' => 'link_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function link_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $link = get_field('url');
    $context['link'] = $link;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/link/link.twig', $context );
}