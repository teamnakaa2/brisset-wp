<?php
add_action( 'acf/init', 'slider_effect_init' );

function slider_effect_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }
    // Register a new block.
    acf_register_block( array(
        'name'            => 'slider_effect',
        'title'           => __( 'Slider 3D', 'your-text-domain' ),
        'render_callback' => 'slider_effect_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        ),
//        'enqueue_assets' => function(){
//            wp_enqueue_style( 'slider-effect', get_template_directory_uri() . '/blocks/slider-effect/slider-effect.css');
//            wp_enqueue_script( 'slider-effect', get_template_directory_uri() . '/blocks/slider-effect/slider-effect.js', array(), '', true );
//        },
    ));
}

function slider_effect_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();
    // Store block values.
    $context['block'] = $block;
    
    // Store field values.
    $slider_effect = get_field('slider_effect');
    $context['slider_effect'] = $slider_effect;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/slider-effect/slider-effect.twig', $context );
}