// import {BehaviorSubject, from, fromEvent, merge, Subject, timer} from "rxjs";
// const {BehaviorSubject, from, fromEvent, merge, Subject, timer} = require("rxjs");
// import {map, mergeMap, scan, switchMap, tap, withLatestFrom} from "rxjs/operators";
// const {map, mergeMap, scan, switchMap, tap, withLatestFrom} = require("rxjs/operators");

console.log('YYEEESSS');
/* Slider 3D */

function domReady(fn) {
    // If we're early to the party
    document.addEventListener("DOMContentLoaded", fn);
    // If late; I mean on time.
    if (document.readyState === "interactive" || document.readyState === "complete" ) {
        fn();
    }
}

domReady(() => {
    var Conclave=(function() {
        var buArr = [], arlen, hasbeenclicked;
        const clickTrigger$ = new Subject();
        const currentSlide$ = new BehaviorSubject(4);
        const pause$ = new BehaviorSubject(true);
        const interval = 5000;
        return {
            init: function () {
                hasbeenclicked = false;
                this.addCN();
                this.clickReg();
                this.auto();
            },
            addCN: function () {
                var buarr = ["holder_bu_awayL2", "holder_bu_awayL1", "holder_bu_center", "holder_bu_awayR1", "holder_bu_awayR2"];
                for (var i = 1; i <= buarr.length; ++i) {
                    let item = document.getElementById('bu' + i);
                    item.setAttribute('class', buarr[i - 1] + " holder_bu")
                }
            },
            clickReg: function () {
                Array.from(document.getElementsByClassName('holder_bu')).forEach(
                    function(element, index, array) {
                        buArr.push(element.getAttribute('class'));
                    }
                );
                arlen = buArr.length;
                for (var i = 0; i < arlen; ++i) {
                    buArr[i] = buArr[i].replace(" holder_bu", "");
                }

                const slides = Array.from(document.querySelectorAll('.holder_bu'));
                const clickEventObs$ = merge(
                    clickTrigger$,
                    from(slides).pipe(
                        mergeMap((slide) => fromEvent(slide, 'click')),
                        map((buid) => buid.target.offsetParent.id),
                        tap((buid) => pause$.next(true))
                    )
                );
                clickEventObs$.subscribe((buid) => {
                    // Set current slide
                    const nextSlide = (parseInt(buid.substring(2)) % 5) + 1;
                    currentSlide$.next(nextSlide);
                    var me = this, id = buid, joId = document.getElementById(id);
                    if (!joId) { return; }
                    const joCN = joId.classList.value.replace(" holder_bu","");
                    var cpos = buArr.indexOf(joCN), mpos = buArr.indexOf("holder_bu_center");
                    if (cpos != mpos) {
                        var tomove;
                        tomove = cpos > mpos ? arlen - cpos + mpos : mpos - cpos;
                        while (tomove) {
                            var t = buArr.shift();
                            buArr.push(t);
                            for (var i = 1; i <= arlen; ++i) {
                                document.getElementById("bu" + i).setAttribute('class', buArr[i - 1] + " holder_bu");
                            }
                            --tomove;
                        }
                    }
                });

            },
            auto: function () {
                pause$.pipe(
                    switchMap((stopped) => (stopped ? timer(interval, interval) : timer(0, interval))),
                    scan((acc) => acc + 1, 0),
                    withLatestFrom(currentSlide$)
                ).subscribe(([val, current]) => {
                    // const x = (val%5)+1;
                    clickTrigger$.next('bu' + current);
                });
            }
        };
    })();
    const slides = Array.from(document.querySelectorAll('.wrapper_bu'));
    if (slides.length) {
        window['conclave']=Conclave;
        Conclave.init();
    }

});
