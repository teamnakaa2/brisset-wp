<?php
add_action( 'acf/init', 'slider_client_logo_init' );

function slider_client_logo_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'slider_client_logo',
        'title'           => __( 'Slider logo client', 'your-text-domain' ),
        'render_callback' => 'slider_client_logo_render_callback',
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function slider_client_logo_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $slider_client_logo = get_field('slider_client_logo');
    $context['slider_client_logo'] = $slider_client_logo;

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/slider-client-logo/slider-client-logo.twig', $context );
}