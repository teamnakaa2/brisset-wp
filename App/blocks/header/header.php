<?php
add_action( 'acf/init', 'header_init' );

function header_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'header',
        'title'           => __( 'Header', 'your-text-domain' ),
        'render_callback' => 'header_render_callback',
        'category'        => 'formatting',
        'icon'            => 'embed-generic',
        'keywords'        => array( 'header', 'entete' ),
        'supports'			=> array(
            'align' => true,
            'mode' => true,
            'jsx' => true
        )
    ));
}

function header_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['header'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/header/header.twig', $context );
}