<?php

use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Textarea;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'Block Texte simple',
    'supports' => array(
        'align' => false,

    ),
    'align' => false,
    'fields' => [
        Textarea::make('Texte', 'txt')
            ->newLines('br') // br or wpautop
            ->characterLimit(600)
            ->rows(10),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/block-txt-simple')
    ],
]);

add_action( 'acf/init', 'my_block_txt_simple_init' );
function my_block_txt_simple_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }
    // Register a new block.
    acf_register_block( array(
        'name'            => 'block_txt_simple',
        'title'           => __( 'Block Text Simple', 'your-text-domain' ),
        'render_callback' => 'my_block_txt_simple_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' )
    ) );
}

function my_block_txt_simple_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['text'] = get_field('txt');

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-txt-simple/block-txt-simple.twig', $context );
}