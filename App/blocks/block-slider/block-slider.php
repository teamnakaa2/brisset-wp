<?php

use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Repeater;

register_extended_field_group([
    'style' => 'seamless',
    'label_placement' => 'top',
    'title' => 'BlockSlider',
    'supports' => array( 
        'align' => false,

     ),
     'align' => false,
    'fields' => [
        Text::make('Titre'),
        Text::make('Sous-titre'),
        Repeater::make('Slider')
            ->instructions('Ajouter un élément au slider.')
            ->fields([
                Text::make('Slide name', 'name'),
                Image::make('Slide Picture', 'img'),
            ])
            ->min(1)
            // ->collapsed('name')
            ->buttonLabel('Ajouter un élément')
            ->layout('block') // block, row or table
            ->required()
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/slider-block')
    ],
]);

add_action( 'acf/init', 'my_slider_init' );
function my_slider_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'slider_block',
        'title'           => __( 'Slider Block', 'your-text-domain' ),
        'description'     => __( 'A slider block.', 'your-text-domain' ),
        'render_callback' => 'my_slider_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'example' ),
        'enqueue_style' => '/wp-content/assets/block-slider.css',
//        'enqueue_script' => '/wp-content/assets/block-slider.js',

    ) );
}

function my_slider_block_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['sliderFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-slider/block-slider.twig', $context );
}