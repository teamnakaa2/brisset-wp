<?php

use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Textarea;
use WordPlate\Acf\Fields\Group;
use WordPlate\Acf\Fields\Link;


register_extended_field_group([
    'title' => 'HeaderHP',
    'style' => 'seamless',
    'label_placement' => 'top',
    'supports' => array( 
        'align' => false,

     ),
     'align' => false,
     'fields' => [
        Image::make('Image', 'img'),
        Text::make('Titre', 'title'),
        Textarea::make('Text', 'txt'),
        Group::make('Groupe bouton' , 'listBtn')
            ->fields([
               Link::make('Lien 1'),
               Link::make('Lien 2')
            ])
            ->layout('row'),
    ],
    'label_placement' => 'top',
    'position' => 'normal',
    'location' => [
        Location::if('block', 'acf/header-hp-block')
    ],
]);

add_action( 'acf/init', 'my_header_hp_init' );
function my_header_hp_init() {
    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a new block.
    acf_register_block( array(
        'name'            => 'header_hp_block',
        'title'           => __( 'Header HP Block', 'your-text-domain' ),
        'description'     => __( 'A header hp block.', 'your-text-domain' ),
        'render_callback' => 'my_header_hp_render_callback',
        'category'        => 'formatting',
        'icon'            => 'embed-generic',
        'keywords'        => array( 'header', 'entete' ),
//        'enqueue_style' => '/wp-content/assets/block-header-hp.css',
    ) );
}

function my_header_hp_render_callback( $block, $content = '', $is_preview = false ) {
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['headerHpFields'] = get_fields();

    // Store $is_preview value.
    $context['is_preview'] = $is_preview;

    // Render the block.
    Timber::render( './blocks/block-header-hp/block-header-hp.twig', $context );
}