<?php

/*
*	Theme config
*/

require_once(ABSPATH . '/vendor/autoload.php' );

// Require Timber config
require get_template_directory() . '/_include/Timber.php';

// Require reset configs
require get_template_directory() . '/_include/assets/reset.php';
// Require assets configs
require get_template_directory() . '/_include/assets/hash.php';
require get_template_directory() . '/_include/assets/style.php';
require get_template_directory() . '/_include/assets/script.php';

// Require posts
require get_template_directory() . '/_include/posts/post-legal.php';
require get_template_directory() . '/_include/posts/post-client.php';
require get_template_directory() . '/_include/posts/post-team.php';

// Require menu configs
require get_template_directory() . '/_include/menu.php';


// Require ACF models
require get_template_directory() . '/models/theme-settings.php';
require get_template_directory() . '/models/contact.php';
require get_template_directory() . '/models/post-client.php';
require get_template_directory() . '/models/post-team.php';
require get_template_directory() . '/models/menu.php';

// Require ACF options page
require get_template_directory() . '/_include/acf-options-page.php';



// Gestion des blocks

// Require model
require get_template_directory() . '/models/Blocks.php';

// Require ACF init


require get_template_directory() . '/blocks/block-slider/block-slider.php';
// require get_template_directory() . '/blocks/block-header/block-header.php';
require get_template_directory() . '/blocks/block-section/block-section.php';
require get_template_directory() . '/blocks/block-mask-img/block-mask-img.php';
require get_template_directory() . '/blocks/block-list/block-list.php';
require get_template_directory() . '/blocks/block-col-icon/block-col-icon.php';
require get_template_directory() . '/blocks/block-col-image-mask/block-col-image-mask.php';
require get_template_directory() . '/blocks/block-form/block-form.php';
require get_template_directory() . '/blocks/block-history/block-history.php';
require get_template_directory() . '/blocks/block-grid-masonry/block-grid-masonry.php';
require get_template_directory() . '/blocks/block-header-hp/block-header-hp.php';
require get_template_directory() . '/blocks/block-header-small/block-header-small.php';
require get_template_directory() . '/blocks/block-breadcrumb/block-breadcrumb.php';
require get_template_directory() . '/blocks/block-wysiwyg/block-wysiwyg.php';
require get_template_directory() . '/blocks/block-txt-simple/block-txt-simple.php';


require get_template_directory() . '/blocks/text-asset/text-asset.php';
require get_template_directory() . '/blocks/title-text-asset/title-text-asset.php';
require get_template_directory() . '/blocks/list-text/list-text.php';


function list_allowed_block_types() {
    return array(
        'core/spacer',
        'core/image',
        'core/paragraph',
        'core/heading',
        'core/list',
        'core/cover',
        'core/gallery',
        'core/video',
        'core/text-columns',
        'core/columns',
        'core/shortcode',
        'acf/slider-block',
        'acf/header-block',
        'acf/section-block',
        'acf/mask-img-block',
        'acf/list-block',
        'acf/col-icon-block',
        'acf/col-image-mask-block',
        'acf/form-block',
        'acf/history-block',
        'acf/image-text-block',
        'acf/grid-masonry-block',
        'acf/header-hp-block',
        'acf/header-small-block',
        'acf/text-asset',
        'acf/title-text-asset',
        'acf/list-text',
        'acf/slider-client-logo',
        'acf/slider-client-avis',
        'acf/list-equipe',
        'acf/table-comparative',
        'acf/list-step',
        'acf/link',
        'acf/slider-effect',
        'acf/block-breadcrumb',
        'acf/block-wysiwyg',
        'acf/block-txt-simple',
        'acf/header'
    );
}
// Liste des blocks
function importBlocks($name) {
    return require get_template_directory() . '/blocks/'.$name.'/'.$name.'.php';
}
importBlocks('slider-client-logo');
importBlocks('slider-client-avis');
importBlocks('list-equipe');
importBlocks('table-comparative');
importBlocks('list-step');
importBlocks('link');
importBlocks('slider-effect');
//importBlocks('block-breadcrumb');
importBlocks('header');

add_filter( 'allowed_block_types_all', 'list_allowed_block_types' );



// add_filter( 'render_block', 'wrap_heading_block', 10, 2 );
// function wrap_heading_block( $block_content, $block ) {
// 	if ( 'core/heading' !== $block['blockName'] ) {
// 		return $block_content;
//     }
//     $return = '';
//     // if (strpos($block_content, 'id') !== false) {
//     //     $return  .= '<div data-aos="fade-up">';
//     // }
// 	$return .= $block_content;
//     // if (strpos($block_content, 'id') !== false) {
//     //     $return .= '</div>';
//     // }

// 	return $return;
// }
add_filter( 'render_block', 'wrap_paragraph_block', 10, 2 );
function wrap_paragraph_block( $block_content, $block ) {
	if ( 'core/paragraph' !== $block['blockName'] ) {
		return $block_content;
    }
	$return  = '<div data-aos="fade-up">';
	$return .= $block_content;
	$return .= '</div>';

	return $return;
}
add_filter( 'render_block', 'wrap_shortcode_block', 10, 2 );
function wrap_shortcode_block( $block_content, $block ) {
	if ( 'core/shortcode' !== $block['blockName'] ) {
		return $block_content;
	}
	$return  = '<div class="wrapper breadcrumb">';
	$return .= $block_content;
	$return .= '</div>';

	return $return;
}