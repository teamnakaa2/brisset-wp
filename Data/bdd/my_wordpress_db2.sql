-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Linux (x86_64)
--
-- Host: bdd    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_yoast_indexable`
--

DROP TABLE IF EXISTS `wp_yoast_indexable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_yoast_indexable` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` longtext COLLATE utf8mb4_unicode_520_ci,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `object_id` int(11) unsigned DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `author_id` int(11) unsigned DEFAULT NULL,
  `post_parent` int(11) unsigned DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_520_ci,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT '0',
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) unsigned DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_520_ci,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT '0',
  `is_robots_noindex` tinyint(1) DEFAULT '0',
  `is_robots_nofollow` tinyint(1) DEFAULT '0',
  `is_robots_noarchive` tinyint(1) DEFAULT '0',
  `is_robots_noimageindex` tinyint(1) DEFAULT '0',
  `is_robots_nosnippet` tinyint(1) DEFAULT '0',
  `twitter_title` text COLLATE utf8mb4_unicode_520_ci,
  `twitter_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_title` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  `language` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  KEY `object_id_and_type` (`object_id`,`object_type`),
  KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_yoast_indexable`
--

LOCK TABLES `wp_yoast_indexable` WRITE;
/*!40000 ALTER TABLE `wp_yoast_indexable` DISABLE KEYS */;
INSERT INTO `wp_yoast_indexable` VALUES (1,'http://localhost/author/wordpress/','34:d1163f75ce62f95c34239405d2eb4d2f',1,'user',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'https://1.gravatar.com/avatar/11151902dc2a07747440499af733cbd3?s=500&d=mm&r=g',NULL,NULL,'gravatar-image',NULL,NULL,'https://1.gravatar.com/avatar/11151902dc2a07747440499af733cbd3?s=500&d=mm&r=g',NULL,'gravatar-image',NULL,NULL,NULL,NULL,'2020-09-19 16:58:09','2020-09-21 05:59:22',1,NULL,NULL,NULL,NULL,0),(2,'http://localhost/','17:c9db569cb388e160e4b86ca1ddff84d7',36,'post','page',1,0,NULL,NULL,'Brisset Partenaire','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,NULL,NULL,'2020-09-19 16:58:09','2020-09-19 16:58:09',1,NULL,NULL,NULL,NULL,0),(3,NULL,NULL,NULL,'system-page','404',NULL,NULL,'Page non trouvée %%sep%% %%sitename%%',NULL,'Erreur 404 : Page introuvable',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 16:58:11','2020-09-19 16:58:11',1,NULL,NULL,NULL,NULL,0),(4,NULL,NULL,NULL,'system-page','404',NULL,NULL,'Page non trouvée %%sep%% %%sitename%%',NULL,'Erreur 404 : Page introuvable',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 16:58:11','2020-09-19 16:58:11',1,NULL,NULL,NULL,NULL,0),(5,NULL,NULL,NULL,'system-page','404',NULL,NULL,'Page non trouvée %%sep%% %%sitename%%',NULL,'Erreur 404 : Page introuvable',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 16:58:11','2020-09-19 16:58:11',1,NULL,NULL,NULL,NULL,0),(6,NULL,NULL,NULL,'system-page','404',NULL,NULL,'Page non trouvée %%sep%% %%sitename%%',NULL,'Erreur 404 : Page introuvable',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 16:58:11','2020-09-19 16:58:11',1,NULL,NULL,NULL,NULL,0),(7,'http://localhost/marches-prives/','32:7ed0055813a26d529a6d35bb54857019',67,'post','page',1,0,NULL,NULL,'Marchés privés','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,90,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-09-19 16:58:35','2020-09-19 21:12:19',1,NULL,NULL,NULL,NULL,0),(8,'http://localhost/contact/','25:ae91d1a4bff113dd4d76f03da32946ec',79,'post','page',1,0,NULL,NULL,'Contact','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,30,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,NULL,'2020-09-19 17:01:08','2020-09-19 21:03:14',1,NULL,NULL,NULL,NULL,0),(9,'http://localhost/la-solution-brisset-partenaires/','49:f85373cc8c62c65c86fe3c6e01f5d906',64,'post','page',1,0,NULL,NULL,'La solution Brisset Partenaires','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,90,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,'2020-09-19 17:01:08','2020-09-19 21:04:30',1,NULL,NULL,NULL,NULL,0),(10,'http://localhost/marches-public/','32:ad0018c0f34c6cf256ad0246ea3a8ef2',70,'post','page',1,0,NULL,NULL,'Marchés public','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,90,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-09-19 17:01:09','2020-09-19 21:11:48',1,NULL,NULL,NULL,NULL,0),(11,'http://localhost/qui-somme-nous/','32:0964beaca197042cc3351993cbef4044',77,'post','page',1,0,NULL,NULL,'Qui somme-nous ?','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,60,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL,NULL,'2020-09-19 17:01:09','2020-09-19 21:05:06',1,NULL,NULL,NULL,NULL,0),(12,'http://localhost/','17:c9db569cb388e160e4b86ca1ddff84d7',NULL,'home-page',NULL,NULL,NULL,'%%sitename%% %%page%% %%sep%% %%sitedesc%%','Just another WordPress site','Accueil',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,NULL,NULL,5,NULL,'2020-09-19 17:01:16','2020-09-19 21:05:04',1,NULL,NULL,NULL,NULL,0),(13,'http://localhost/pagelegale/','28:122f4d0c8405aeb892129fd42d0a80a2',NULL,'post-type-archive','pagelegale',NULL,NULL,'%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%','','Pages Légales',NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 17:01:16','2020-09-19 17:01:16',1,NULL,NULL,NULL,NULL,0),(14,'http://localhost/?post_type=team','32:55a018ffbfbc9c4312dff17a5b4da534',NULL,'post-type-archive','team',NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 17:01:16','2020-09-19 17:01:16',1,NULL,NULL,NULL,NULL,0),(15,'http://localhost/client/','24:0325b1569739c9863caaa02aa276e10e',NULL,'post-type-archive','client',NULL,NULL,'%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%','','Clients',NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 17:01:16','2020-09-19 17:01:16',1,NULL,NULL,NULL,NULL,0),(16,'http://localhost/wp-content/uploads/2020/09/public.png','54:76fa402699fef9faada1d388157aeb4a',396,'post','attachment',1,70,NULL,NULL,'public','inherit',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,'http://localhost/wp-content/uploads/2020/09/public.png',NULL,'396','attachment-image',NULL,NULL,NULL,'396','attachment-image',NULL,0,NULL,NULL,'2020-09-19 21:09:33','2020-09-19 21:09:33',1,NULL,NULL,NULL,NULL,1),(17,'http://localhost/wp-content/uploads/2020/09/Groupe-3772.png','59:b16edd8b7c9bfbb22c94858df1f6ccd6',398,'post','attachment',1,70,NULL,NULL,'Groupe 3772','inherit',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,'http://localhost/wp-content/uploads/2020/09/Groupe-3772.png',NULL,'398','attachment-image',NULL,NULL,NULL,'398','attachment-image',NULL,0,NULL,NULL,'2020-09-19 21:11:08','2020-09-19 21:11:08',1,NULL,NULL,NULL,NULL,1),(18,'http://localhost/wp-content/uploads/2020/09/prive.png','53:183122ad9aebadf55ef116635627cf73',401,'post','attachment',1,67,NULL,NULL,'prive','inherit',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,'http://localhost/wp-content/uploads/2020/09/prive.png',NULL,'401','attachment-image',NULL,NULL,NULL,'401','attachment-image',NULL,0,NULL,NULL,'2020-09-19 21:12:09','2020-09-19 21:12:09',1,NULL,NULL,NULL,NULL,1),(19,'http://localhost/pagelegale/mentions-rgpd/','42:1ffaa312d4a5170307f9f71ad2932b7e',76,'post','pagelegale',1,0,NULL,NULL,'Mentions RGPD','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'2020-09-21 05:46:01','2020-09-21 05:46:01',1,NULL,NULL,NULL,NULL,0),(20,'http://localhost/author/','24:8a021a94301ea0550218bff88af1607f',0,'user',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'https://2.gravatar.com/avatar/?s=500&d=mm&r=g',NULL,NULL,'gravatar-image',NULL,NULL,'https://2.gravatar.com/avatar/?s=500&d=mm&r=g',NULL,'gravatar-image',NULL,NULL,NULL,NULL,'2020-09-21 05:53:50','2020-09-21 06:01:05',1,NULL,NULL,NULL,NULL,0),(21,'http://localhost/?post_type=wpcf7_contact_form&p=1','50:1d070af5c1cdede7455bfa1037405030',1,'post','wpcf7_contact_form',0,0,NULL,NULL,'Contact form 1','publish',NULL,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'2020-09-21 05:53:50','2020-09-21 06:01:05',1,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `wp_yoast_indexable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_yoast_indexable_hierarchy`
--

DROP TABLE IF EXISTS `wp_yoast_indexable_hierarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) unsigned NOT NULL,
  `ancestor_id` int(11) unsigned NOT NULL,
  `depth` int(11) unsigned DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`indexable_id`,`ancestor_id`),
  KEY `indexable_id` (`indexable_id`),
  KEY `ancestor_id` (`ancestor_id`),
  KEY `depth` (`depth`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_yoast_indexable_hierarchy`
--

LOCK TABLES `wp_yoast_indexable_hierarchy` WRITE;
/*!40000 ALTER TABLE `wp_yoast_indexable_hierarchy` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_yoast_indexable_hierarchy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-21  7:58:46
