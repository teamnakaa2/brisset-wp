# Lancer le projet

Docs : 
- https://timber.github.io/
- https://github.com/wordplate/extended-acf
- https://michalsnik.github.io/aos/

### Pour installer le projet
- make install

### Pour importer la bdd
- make importbdd

### Pour compiler les assets
- make dev

# Exporter le local
## Pour exporter local et push
- make exportbdd
- git push

# Mettre le projet en re7
## Pour exporter en re7
- make exportre7
- make exportyost
- make build
- importer la bdd sur la re7
- (changer $devMode à false App/_include/assets/script.php)
- (changer $devMode à false App/_include/assets/script.php)

- copier www dans www
- copier App dans www/wp-content/themes/App
- copier Data/images dans www/wp-content/uploads
- modifier les infos de bdd dans le fichier wp-config à la racine
