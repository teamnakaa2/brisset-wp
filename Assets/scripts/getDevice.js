// // class pour déterminer le device uniquement avec le breakpoint, utile uniquement sur desktop
// // En fonction de la taille d'écran je rajoute un data attibue avec le nom du device 
// // body data-device="onSmallDevice"
class getDevice {

  constructor() {
    this.bodyWidth = document.body.clientWidth
    this.isSmallDevice = (this.bodyWidth <= 480) ? true : false
    this.isMediumDevice = (this.bodyWidth > 481 && this.bodyWidth <= 1080) ? true : false
    this.isLargeDevice = (this.bodyWidth > 1081 ) ? true : false
    // this.isLargeDevice = (this.bodyWidth > 1081 && this.bodyWidth >= 1480) ? true : false
  }

  init() {
    this.onSmallDevice()
    this.onMediumDevice()
    this.onLargeDevice()
  }

  onSmallDevice () {
    if(this.isSmallDevice) {
      document.body.setAttribute('data-device','onSmallDevice')
    }
    return this.isSmallDevice
  }

  onMediumDevice () {
    if(this.isMediumDevice) {
      document.body.setAttribute('data-device','onMediumDevice')
    }
    return this.isMediumDevice
    
  }
  
  onLargeDevice () {
    if(this.isLargeDevice) {
      document.body.setAttribute('data-device','onLargeDevice')
    }
    return this.isLargeDevice
  }
  
}

module.exports= getDevice