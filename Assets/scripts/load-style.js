import getDevice from './getDevice'

class loadStyle {

    constructor(log = false) {
        this.head = document.getElementsByTagName('HEAD')[0];
        this.log = log;
        this.onResize();
        this.addHeadLinkStyle();
    }

    onResize() {
        let resizeTimer;
        window.addEventListener('resize', e => {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout( ()=> {
               this.addHeadLinkStyle()
            }, 300);
        })
    }

    urlStyle(addStyle) {
        const initDevice = new getDevice()
        // if (this.log) console.log(initDevice);

        if(addStyle) {
            if (this.log) console.log('urlStyle' + addStyle);
            return 'wp-content/assets/' + addStyle;
        }

        if (initDevice.isSmallDevice) {
            if (this.log) console.log('small');
            return 'http://jonestudio.local/wp-content/assets/index.mobile.index.css';
        }

        if (initDevice.isMediumDevice) {
            if (this.log) console.log('medium');
            return 'http://jonestudio.local/wp-content/assets/index.tablet.index.css';
        }

        if (initDevice.isLargeDevice) {
            if (this.log) console.log('large');
            return 'http://jonestudio.local/wp-content/assets/index.desktop.index.css';
        }
    }

    isStyleLoaded() {
        return (document.querySelector('[href="'+this.urlStyle()+'" ]')) ?  true : false;
    }

    addHeadLinkStyle(addUrlStyle) {
        console.log("dkaozd");
        if (!this.isStyleLoaded()) {
            const link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = this.urlStyle(addUrlStyle);
            this.head.appendChild(link);
        }
    }

    addStyle(newUrl) {
       return this.addHeadLinkStyle(newUrl);
    }

}

export default loadStyle;