import Glide from '@glidejs/glide'
import AOS from 'aos';
import 'aos/dist/aos.css';
import { BehaviorSubject, Subject, timer } from 'rxjs';
import { fromEvent, from , merge } from 'rxjs';
import { map, mergeMap, scan, switchMap, withLatestFrom, tap } from 'rxjs/operators';

const titles = document.querySelectorAll('h2');
titles.forEach((title) => {
  title.setAttribute('data-aos', 'fade-up')
})

AOS.init();



const btnNavOpen = document.getElementById('btn-nav-open')
btnNavOpen.onclick = function() {
  document.body.classList.add("navIsOpen")
}
const btnNavClsoe = document.getElementById('btn-nav-close')
btnNavClsoe.onclick = function() {
  document.body.classList.remove("navIsOpen")
}

const sliderClientAvis = document.querySelector('#slider-client-avis.glide');
sliderClientAvis && new Glide(sliderClientAvis , {
  perView: 1
}).mount()

const sliderClientLogo = document.querySelector('#slider-client-logo.glide');
sliderClientLogo && new Glide(sliderClientLogo , {
  perView: 4,
  breakpoints: {
    480: {
      perView: 3
    }
  }
}).mount()

const sliderEquipe = document.querySelector('#slider-list-equipe.glide');
sliderEquipe && new Glide(sliderEquipe , {
  perView: 2,
  breakpoints: {
    480: {
      perView: 1
    }
  }
}).mount();


const links = document.querySelectorAll('a#gocontent');
 
for (const link of links) {
  link.addEventListener("click", clickHandler);
}
 
function clickHandler(e) {
  const href = this.getAttribute("href");
  if(href.includes('#')) {
    e.preventDefault();
    const offsetTop = document.querySelector(href).offsetTop;
   
    scroll({
      top: offsetTop,
      behavior: "smooth"
    });
  }
}




/* Slider 3D */

function domReady(fn) {
  // If we're early to the party
  document.addEventListener("DOMContentLoaded", fn);
  // If late; I mean on time.
  if (document.readyState === "interactive" || document.readyState === "complete" ) {
    fn();
  }
}

domReady(() => {
  var Conclave=(function() {
    var buArr = [], arlen, hasbeenclicked;
    const clickTrigger$ = new Subject();
    const currentSlide$ = new BehaviorSubject(4);
    const pause$ = new BehaviorSubject(true);
    const interval = 5000;
    return {
      init: function () {
        hasbeenclicked = false;
        this.addCN();
        this.clickReg();
        this.auto();
      },
      addCN: function () {
        var buarr = ["holder_bu_awayL2", "holder_bu_awayL1", "holder_bu_center", "holder_bu_awayR1", "holder_bu_awayR2"];
        for (var i = 1; i <= buarr.length; ++i) {
          let item = document.getElementById('bu' + i);
          item.setAttribute('class', buarr[i - 1] + " holder_bu")
        }
      },
      clickReg: function () {
        Array.from(document.getElementsByClassName('holder_bu')).forEach(
            function(element, index, array) {
              buArr.push(element.getAttribute('class'));
            }
        );
        arlen = buArr.length;
        for (var i = 0; i < arlen; ++i) {
          buArr[i] = buArr[i].replace(" holder_bu", "");
        }

        const slides = Array.from(document.querySelectorAll('.holder_bu'));
        const clickEventObs$ = merge(
            clickTrigger$,
            from(slides).pipe(
                mergeMap((slide) => fromEvent(slide, 'click')),
                map((buid) => buid.target.offsetParent.id),
                tap((buid) => pause$.next(true))
            )
        );
        clickEventObs$.subscribe((buid) => {
          // Set current slide
          const nextSlide = (parseInt(buid.substring(2)) % 5) + 1;
          currentSlide$.next(nextSlide);
          var me = this, id = buid, joId = document.getElementById(id);
          if (!joId) { return; }
          const joCN = joId.classList.value.replace(" holder_bu","");
          var cpos = buArr.indexOf(joCN), mpos = buArr.indexOf("holder_bu_center");
          if (cpos != mpos) {
            var tomove;
            tomove = cpos > mpos ? arlen - cpos + mpos : mpos - cpos;
            while (tomove) {
              var t = buArr.shift();
              buArr.push(t);
              for (var i = 1; i <= arlen; ++i) {
                document.getElementById("bu" + i).setAttribute('class', buArr[i - 1] + " holder_bu");
              }
              --tomove;
            }
          }
        });

      },
      auto: function () {
        pause$.pipe(
            switchMap((stopped) => (stopped ? timer(interval, interval) : timer(0, interval))),
            scan((acc) => acc + 1, 0),
            withLatestFrom(currentSlide$)
        ).subscribe(([val, current]) => {
          // const x = (val%5)+1;
          clickTrigger$.next('bu' + current);
        });
      }
    };
  })();
  const slides = Array.from(document.querySelectorAll('.wrapper_bu'));
  if (slides.length) {
    window['conclave']=Conclave;
    Conclave.init();
  }

});
