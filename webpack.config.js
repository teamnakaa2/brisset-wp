var path = require('path');
var glob = require("glob")
var webpack = require('webpack');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MediaQueryPlugin = require('media-query-plugin');

 module.exports = {
     mode: 'development',
     devtool:  (process.env.NODE_ENV == 'production') ? 'inline-source-map' : 'inline-source-map',
     entry: {
       'admin': [path.resolve(__dirname, './Assets/styles/admin.scss')],
       'global': [path.resolve(__dirname, './Assets/styles/global.scss')],
       'index': [path.resolve(__dirname, './Assets/scripts/index.js'), path.resolve(__dirname, './Assets/styles/index.scss')],
       'p-contact': [path.resolve(__dirname, './Assets/styles/p-contact.scss')],
       'block-global': glob.sync('./App/blocks/**/*.scss')
     },
     optimization: {
        minimize: false
    },
     output: {
        path: path.resolve('./www/wp-content/assets'),
        filename: '[name].js',
     },
     module: {
        rules: [
          { test: /\.js$/, exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          },
          { 
            test: /\.(s?)css$/, 
            use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { sourceMap: true } },
            { loader: 'postcss-loader', options: { sourceMap: true } },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                sassOptions: {
                  indentWidth: 4,
                },
              },
            },
            'import-glob-loader'
            ]
          },
          {
            test: /\.(ico|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
            use: {
                loader: 'file-loader',
                options: {
                  name: 'fonts/[name].[ext]'
                }
              }
          }
        ]
      },
      plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin(),
        new webpack.HotModuleReplacementPlugin()
      ],
      // devServer: {
      //   contentBase: path.resolve('./www/wp-content/assets'),
      //   host: '0.0.0.0',
      //   hot: true,
      //   inline: true,
      //   overlay: true,
      //   port: 5000,
      // }
};